// Errors defines common errors.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

// objectError implements an error.
type objectError int

// Common errors.
const (
	ErrArg1NotARational = objectError(iota)
	ErrArg2NotARational
	ErrArgNotARational
	ErrDivisionByZero
	ErrDecodingIntoNilObject
	ErrDecodingIntoExistingObject
	ErrEmptySlice
	ErrOddLengthSlice
	ErrOutOfRange
	ErrRationalNotAnInteger
	ErrStringNotARational
)

/////////////////////////////////////////////////////////////////////////
// objectError functions
/////////////////////////////////////////////////////////////////////////

// Error returns the error message.
func (e objectError) Error() string {
	switch e {
	case ErrArg1NotARational:
		return "argument 1 is not a rational"
	case ErrArg2NotARational:
		return "argument 2 is not a rational"
	case ErrArgNotARational:
		return "argument is not an rational"
	case ErrDivisionByZero:
		return "division by zero"
	case ErrDecodingIntoNilObject:
		return "decoding into nil object"
	case ErrDecodingIntoExistingObject:
		return "decoding into existing object"
	case ErrEmptySlice:
		return "illegal empty slice"
	case ErrOddLengthSlice:
		return "the slice should be of even length"
	case ErrOutOfRange:
		return "result of calculation out of range"
	case ErrRationalNotAnInteger:
		return "the rational number cannot be converted to an integer"
	case ErrStringNotARational:
		return "the string cannot be converted to a rational"
	default:
		return "unknown error"
	}
}

// Is return true iff target is equal to e.
func (e objectError) Is(target error) bool {
	ee, ok := target.(objectError)
	return ok && ee == e
}
