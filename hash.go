// Hash contains hashing functions for integers.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcasmath/object"
	"math"
	"math/big"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// hashInt64 returns the hash of the given integer.
func hashInt64(c int64) uint32 {
	return object.CombineHash(uint32(c), uint32(c>>32))
}

// hashBigRat returns the hash of the given big rational.
func hashBigRat(q *big.Rat) uint32 {
	x := big.NewInt(math.MaxInt64)
	y := big.NewInt(0).Mod(q.Num(), x)
	h := hashInt64(y.Int64())
	if !q.IsInt() {
		y.Mod(q.Denom(), x)
		h = object.CombineHash(h, hashInt64(y.Int64()))
	}
	return h
}
