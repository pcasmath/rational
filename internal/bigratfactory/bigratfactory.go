// Bigratfactory maintains a factory of *big.Rats.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package bigratfactory

import (
	"math/big"
	"sync"
)

// pool is a pool of *big.Rats.
var pool = &sync.Pool{
	New: func() interface{} {
		return &big.Rat{}
	},
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new *big.Rat.
func New() *big.Rat {
	return pool.Get().(*big.Rat)
}

// Reuse makes the *big.Rat n available for reuse. Warning: Do NOT use n after making it available for reuse.
func Reuse(n *big.Rat) {
	if n != nil {
		pool.Put(n)
	}
}
