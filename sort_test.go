// Tests for sort.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcasmath/object"
	"github.com/stretchr/testify/assert"
	"testing"
)

func areEqualSlices(t *testing.T, R object.Parent, S []*Element, T []*Element) {
	if len(S) != len(T) {
		t.Fatalf("slices not equal: expected length %d but got length %d", len(T), len(S))
	}
	for i, s := range S {
		ok, err := R.AreEqual(s, T[i])
		if err != nil {
			t.Fatalf("slices not equal: elements at index %d not equal: %s, %s: %v", i, s, T[i], err)
		} else if !ok {
			t.Fatalf("slices not equal: elements at index %d not equal: %s, %s", i, s, T[i])
		}
	}
}

func areEqualObjectSlices(t *testing.T, R object.Parent, S []object.Element, T []*Element) {
	if len(S) != len(T) {
		t.Fatalf("slices not equal: expected length %d but got length %d", len(T), len(S))
	}
	for i, s := range S {
		ok, err := R.AreEqual(s, T[i])
		if err != nil {
			t.Fatalf("slices not equal: elements at index %d not equal: %s, %s: %v", i, s, T[i], err)
		} else if !ok {
			t.Fatalf("slices not equal: elements at index %d not equal: %s, %s", i, s, T[i])
		}
	}
}

// TestSort tests Sort
func TestSort(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some slices of rationals
	//////////////////////////////////////////////////////////////////////
	// empty slices
	emptySlice := make([]*Element, 0)
	emptySliceObj := make([]object.Element, 0)
	// [-3/2]
	lengthOneSlice := make([]*Element, 1)
	if x, err := FromIntPair(-3, 2); assert.NoError(err) {
		lengthOneSlice[0] = x
	}
	lengthOneSliceObj := make([]object.Element, 0, len(lengthOneSlice))
	for _, x := range lengthOneSlice {
		lengthOneSliceObj = append(lengthOneSliceObj, x)
	}
	// [1/2, -2/3]
	lengthTwoSlice := make([]*Element, 2)
	if x, err := FromIntPair(1, 2); assert.NoError(err) {
		lengthTwoSlice[0] = x
	}
	if x, err := FromIntPair(-2, 3); assert.NoError(err) {
		lengthTwoSlice[1] = x
	}
	lengthTwoSliceObj := make([]object.Element, 0, len(lengthTwoSlice))
	for _, x := range lengthTwoSlice {
		lengthTwoSliceObj = append(lengthTwoSliceObj, x)
	}
	// [-2/3, 1/2]
	otherLengthTwoSlice := make([]*Element, 2)
	if x, err := FromIntPair(-2, 3); assert.NoError(err) {
		otherLengthTwoSlice[0] = x
	}
	if x, err := FromIntPair(1, 2); assert.NoError(err) {
		otherLengthTwoSlice[1] = x
	}
	otherLengthTwoSliceObj := make([]object.Element, 0, len(otherLengthTwoSlice))
	for _, x := range otherLengthTwoSlice {
		otherLengthTwoSliceObj = append(otherLengthTwoSliceObj, x)
	}
	// short slices with entries (-1)^i/(i+1)
	shortSlice := make([]*Element, 0, 100)
	for i := 0; i < 100; i++ {
		x, err := FromInt(-1).PowerInt64(int64(i))
		assert.NoError(err)
		if y, err := Divide(x, FromInt(i+1)); assert.NoError(err) {
			shortSlice = append(shortSlice, y)
		}
	}
	shortSliceObj := make([]object.Element, 0, len(shortSlice))
	for _, x := range shortSlice {
		shortSliceObj = append(shortSliceObj, x)
	}
	// long slices made up of [(-1)^i/(i+1) : [i in 0..99]] repeated 100 times
	longSlice := make([]*Element, 0, 10000)
	for i := 0; i < 100; i++ {
		longSlice = append(longSlice, shortSlice...)
	}
	longSliceObj := make([]object.Element, 0, len(longSlice))
	for _, x := range longSlice {
		longSliceObj = append(longSliceObj, x)
	}
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	// the empty slice
	assert.Equal(Sort(emptySlice), emptySlice)
	if S, err := qQ.Sort(emptySliceObj); assert.NoError(err) {
		assert.Equal(S, emptySliceObj)
	}
	// the length one case
	areEqualSlices(t, qQ, Sort(lengthOneSlice), lengthOneSlice)
	if S, err := qQ.Sort(lengthOneSliceObj); assert.NoError(err) {
		areEqualObjectSlices(t, qQ, S, lengthOneSlice)
	}
	// the length two case
	expected := []*Element{lengthTwoSlice[1], lengthTwoSlice[0]} // [-2/3, 1/2]
	areEqualSlices(t, qQ, Sort(lengthTwoSlice), expected)
	if S, err := qQ.Sort(lengthTwoSliceObj); assert.NoError(err) {
		areEqualObjectSlices(t, qQ, S, expected)
	}
	areEqualSlices(t, qQ, Sort(otherLengthTwoSlice), expected)
	if S, err := qQ.Sort(otherLengthTwoSliceObj); assert.NoError(err) {
		areEqualObjectSlices(t, qQ, S, expected)
	}
	// the short case
	expected = make([]*Element, 0, 100)
	// build the sorted list
	for i := 0; i < 50; i++ {
		if x, err := FromIntPair(-1, 2*i+2); assert.NoError(err) {
			expected = append(expected, x)
		}
	}
	for i := 49; i >= 0; i-- {
		if x, err := FromIntPair(1, 2*i+1); assert.NoError(err) {
			expected = append(expected, x)
		}
	}
	areEqualSlices(t, qQ, Sort(shortSlice), expected)
	if S, err := qQ.Sort(shortSliceObj); assert.NoError(err) {
		areEqualObjectSlices(t, qQ, S, expected)
	}
	// the long case
	expected = make([]*Element, 0, 10000)
	// build the sorted list
	for i := 0; i < 50; i++ {
		if x, err := FromIntPair(-1, 2*i+2); assert.NoError(err) {
			for j := 0; j < 100; j++ {
				expected = append(expected, x)
			}
		}
	}
	for i := 49; i >= 0; i-- {
		if x, err := FromIntPair(1, 2*i+1); assert.NoError(err) {
			for j := 0; j < 100; j++ {
				expected = append(expected, x)
			}
		}
	}
	areEqualSlices(t, qQ, Sort(longSlice), expected)
	if S, err := qQ.Sort(longSliceObj); assert.NoError(err) {
		areEqualObjectSlices(t, qQ, S, expected)
	}
	// error cases
	_, err := qQ.Sort([]object.Element{nil})
	assert.Error(err)
	longSliceObj[402] = nil
	_, err = qQ.Sort(longSliceObj)
	assert.Error(err)
}
